//
//  main.m
//  Labb2
//
//  Created by Pontus on 2016-02-04.
//  Copyright © 2016 Pontus Hanzén. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
