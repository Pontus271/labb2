//
//  Questions.m
//  Labb2
//
//  Created by Pontus on 2016-02-04.
//  Copyright © 2016 Pontus Hanzén. All rights reserved.
//

#import "Questions.h"


@interface Questions()
@property (nonatomic) NSArray *questions;
@property (nonatomic) NSArray *option1;
@property (nonatomic) NSArray *option2;
@property (nonatomic) NSArray *option3;
@property (nonatomic) NSArray *option4;
@property (nonatomic) NSArray *allOptions;
@property (nonatomic) int randomQuestion;
@property (nonatomic) NSArray *hints;

@end

@implementation Questions

-(instancetype)init {
    self = [super init];
    if(self) {
        self.questions = @[@"How many weeks are there in a year?", @"Which movie came out on valentines day year 2016?", @"In what programming language is this app coded in?", @"Which of the following lakes are the biggest in Sweden?", @"Which sport is the most popular in the world?", @"How many hours are there in a day?", @"What is our galaxy called?", @"Which group made the song The Safety Dance?", @"Which game is currently most played in the world?", @"What is 3 times 6?"];
        self.option1 = @[@"51", @"The fantastic four", @"Java", @"Siljan", @"Golf", @"22", @"The chocolate way", @"Men Without Boots", @"Dota2", @"16"];
        self.option2 = @[@"52", @"Deadpool", @"Objective C", @"Vänern", @"Football", @"24", @"The milky way", @"Men Without Hats", @"League of legends", @"18"];
        self.option3 = @[@"53", @"Avatar", @"Javascript", @"Hjälmaren",    @"Volleyball", @"26", @"The tea way", @"Men Without Socks", @"Counter Strike", @"20"];
        self.option4 = @[@"54", @"Star Wars Episode 8", @"C++", @"Vättern", @"Hockey", @"28", @"The honey way", @"Men Without Pants", @"Diablo", @"22"];
        self.hints = @[@"100-48", @"The title is not alive", @"The creater of Else Heart.Break() teach me in this language", @"Not the deepest one", @"You kick it", @"53-29", @"It's white",@"Something you can wear on your head", @"Someone famous becomes a...", @"Same as 2 times 9"];
        self.allOptions = @[_option1, _option2, _option3, _option4];
    }
    return self;
}


-(NSString*)buttonTextSet:(int)number {
    NSString *theText;
    for(int i = 0; i < _allOptions.count; i++) {
        if(number == i) {
            theText = _allOptions[i][_randomQuestion];
        }
    }
    return theText;
}

-(NSString*)nextQuestions {
    
    _randomQuestion = arc4random() % _questions.count;
    
    return _questions[_randomQuestion];
}

-(NSString*)checkTheText:(NSString*) title {
    for(int i = 0; i < _allOptions.count; i++) {
        if(title == _allOptions[1][_randomQuestion]) {
            return @"Rätt svar!";
        }
    }
    return @"Felaktigt svar!";
}

-(NSString*)theHint {
    return _hints[_randomQuestion];
}


@end
