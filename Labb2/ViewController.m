//
//  ViewController.m
//  Labb2
//
//  Created by Pontus on 2016-02-04.
//  Copyright © 2016 Pontus Hanzén. All rights reserved.
//

#import "ViewController.h"
#import "Questions.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *theQuestion;
@property (strong, nonatomic) Questions *questionsList;
@property (weak, nonatomic) IBOutlet UIButton *answerButton1;
@property (weak, nonatomic) IBOutlet UIButton *answerButton2;
@property (weak, nonatomic) IBOutlet UIButton *answerButton3;
@property (weak, nonatomic) IBOutlet UIButton *answerButton4;
@property (weak, nonatomic) IBOutlet UILabel *rightOrWrong;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestionButton;
@property (weak, nonatomic) IBOutlet UILabel *hintText;

@end

@implementation ViewController

-(Questions*)questionsList{
    if(!_questionsList) {
        _questionsList = [[Questions alloc] init];
    }
    return _questionsList;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self theQuestionGenerator];
    
    
}

- (IBAction)checkCorrect:(id)sender {
    UIButton *tempButton = (UIButton *)sender;
    self.rightOrWrong.text = [self.questionsList checkTheText:tempButton.currentTitle];
}

- (IBAction)nextQuestion:(id)sender {
    [self theQuestionGenerator];
    self.rightOrWrong.text = @"";
}

-(void)theQuestionGenerator {
    self.theQuestion.text = [self.questionsList nextQuestions];
    [self.answerButton1 setTitle:[self.questionsList buttonTextSet:0] forState:UIControlStateNormal];
    [self.answerButton2 setTitle:[self.questionsList buttonTextSet:1] forState:UIControlStateNormal];
    [self.answerButton3 setTitle:[self.questionsList buttonTextSet:2] forState:UIControlStateNormal];
    [self.answerButton4 setTitle:[self.questionsList buttonTextSet:3] forState:UIControlStateNormal];
}

- (IBAction)theHintButton:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hint"
                                                    message:[self.questionsList theHint]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
