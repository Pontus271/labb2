//
//  AppDelegate.h
//  Labb2
//
//  Created by Pontus on 2016-02-04.
//  Copyright © 2016 Pontus Hanzén. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

