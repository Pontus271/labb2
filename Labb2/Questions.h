//
//  Questions.h
//  Labb2
//
//  Created by Pontus on 2016-02-04.
//  Copyright © 2016 Pontus Hanzén. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Questions : NSObject

-(NSString*)buttonTextSet:(int)number;
-(NSString*)nextQuestions;
-(NSString*)checkTheText:(NSString*) title;
-(NSString*)theHint;
@end
